﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string espacios = "  ";
            //Crea un nuevo objeto de ColorDialog
            ColorDialog PaletaColor = new ColorDialog();
            // Mantiene al usuario seleccionar un color personalizado
            PaletaColor.AllowFullOpen = true;
            // Permite al usuario poner ayuda 
            PaletaColor.ShowHelp = true;
            // Abre el dialogo 
            PaletaColor.ShowDialog();
            //Pone el color inicial de el color elegido
            BoxColor.BackColor = PaletaColor.Color;
            RGB.Text = PaletaColor.Color.R.ToString() + espacios + PaletaColor.Color.G.ToString() + espacios + PaletaColor.Color.B.ToString();
            HTML.Text = ColorTranslator.ToHtml(PaletaColor.Color);
            PaletaColor.Color = BoxColor.BackColor;

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

            string espacios = "  ";
            //Crea un nuevo objeto de ColorDialog
            ColorDialog PaletaColor2 = new ColorDialog();
            // Mantiene al usuario seleccionar un color personalizado
            PaletaColor2.AllowFullOpen = true;
            // Permite al usuario poner ayuda 
            PaletaColor2.ShowHelp = true;
            // Abre el dialogo 
            PaletaColor2.ShowDialog();
            //Pone el color inicial de el color elegido
            BoxColor2.BackColor = PaletaColor2.Color;
            RGB2.Text = PaletaColor2.Color.R.ToString() + espacios + PaletaColor2.Color.G.ToString() + espacios + PaletaColor2.Color.B.ToString();
            HTML2.Text = ColorTranslator.ToHtml(PaletaColor2.Color);
            PaletaColor2.Color = BoxColor2.BackColor;
      

        }

        private void button3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Elaborado por: René Patricio Hinojosa Santos");
        }
    }
}
